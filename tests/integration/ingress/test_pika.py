import uuid

import aio_pika


async def test_libs(ctx, acct_profile, queue):
    """
    Run the test without going through the hub to verify the way it works normally
    """
    message = str(uuid.uuid4()).encode()
    connection: aio_pika.RobustConnection = await aio_pika.connect_robust(
        **ctx.acct.connection
    )

    channel: aio_pika.Channel
    async with await connection.channel() as channel:
        exchange = channel.default_exchange
        ack = await exchange.publish(
            aio_pika.Message(message),
            routing_key=ctx.acct.routing_key,
            timeout=ctx.acct.get("timeout"),
        )

        assert ack

    received_message = await queue.get()
    assert received_message.body == message


async def test_publish_direct(hub, ctx, acct_profile, queue):
    """
    Run the test publishing directly to the kafka ingress plugin
    """
    message = str(uuid.uuid4())
    await hub.ingress.pika.publish(ctx, body=message)

    received_message = await queue.get()

    assert received_message.routing_key == ctx.acct.routing_key
    assert received_message.body == message.encode()


async def test_publish_indirect(hub, ctx, evbus_broker, acct_profile, queue):
    """
    Run the test by publishing to the evbus broker and waiting for it to show up in pika
    """
    message = str(uuid.uuid4())

    # Put a message on the evbus broker, it should propagate to pika soon
    hub.evbus.broker.put_nowait(profile=acct_profile, body=message)

    await hub.pop.loop.sleep(1)

    received_message = await queue.get()

    assert received_message.routing_key == ctx.acct.routing_key
    assert received_message.body == f'"{message}"'.encode()
