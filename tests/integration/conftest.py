import asyncio
from unittest import mock

import aio_pika
import dict_tools.data
import pytest


@pytest.fixture(scope="session")
def acct_profile():
    return "test_development_evbus_pika"


@pytest.fixture(autouse=True, scope="function")
async def event_loop(event_loop):
    yield event_loop

    # Cancel all remaining tasks
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]

    for task in tasks:
        task.cancel()

    await asyncio.gather(*tasks, return_exceptions=True)


@pytest.fixture(scope="function", name="hub")
def integration_hub(hub):
    hub.pop.sub.add(dyne_name="evbus")

    with mock.patch("sys.argv", ["evbus"]):
        hub.pop.config.load(["acct", "evbus"], cli="acct", parse_cli=False)

    yield hub


@pytest.fixture(scope="function")
async def contexts(hub, acct_profile):
    # ACCT_FILE needs to be set in the environment
    if not (hub.OPT.acct.acct_file):
        raise pytest.skip(
            f"ACCT_FILE needs to be set in the environment to run this test"
        )

    contexts = await hub.evbus.acct.profiles(
        acct_file=hub.OPT.acct.acct_file,
        acct_key=hub.OPT.acct.acct_key,
    )
    if not contexts:
        raise pytest.skip(f"No profiles collected from '{hub.OPT.acct.acct_file}'")
    contexts = dict_tools.data.NamespaceDict(**contexts)

    yield contexts


@pytest.fixture()
def ctx(contexts, acct_profile):
    profiles = (
        contexts.get("pika", {})
        or contexts.get("amqp", {})
        or contexts.get("rabbitmq", {})
    )
    ctx_acct = None
    for profile in profiles:
        ctx_acct = profile.get(acct_profile)
    if not ctx_acct:
        raise pytest.skip("No profile available for connection")

    yield dict_tools.data.NamespaceDict(acct=ctx_acct)


@pytest.fixture()
async def evbus_broker(hub, contexts):
    task = hub.pop.Loop.create_task(hub.evbus.init.start(contexts))
    await hub.evbus.init.join()

    yield

    # Stop evbus
    await hub.evbus.init.stop()
    await task


@pytest.fixture(scope="function")
async def queue(contexts, acct_profile, event_loop):
    for provider in ("pika", "amqp", "rabbitmq"):
        ctx_acct = None
        for profile in contexts.get(provider, []):
            ctx_acct = profile.get(acct_profile)
            if ctx_acct:
                break
        if ctx_acct:
            break
    else:
        raise pytest.skip("No profile available for connection")

    routing_key = ctx_acct.get("routing_key", "")

    async with await aio_pika.connect(**ctx_acct.connection) as conn:
        async with conn.channel() as channel:
            queue: aio_pika.RobustQueue = await channel.declare_queue(routing_key)
            yield queue
            await queue.delete(if_unused=False, if_empty=False)
